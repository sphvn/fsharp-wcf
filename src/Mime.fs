﻿module Mime

open System.Collections.Generic

let private mimes = new Dictionary<string,string>()
mimes.[".jpg"]  <- "image/jpeg"
mimes.[".png"]  <- "image/png"
mimes.[".gif"]  <- "image/gif"
mimes.[".html"] <- "text/html"
mimes.[".css"]  <- "text/css"
mimes.[".js"]   <- "application/javascript"

let private StartsWith(a:string)(b:char) =
  if (a.[0] = b) then true
  else false

let getMimeType(ext:string) :string =
  let b = ext |> StartsWith <| '.'
  if b then mimes.[ext]
  else mimes.["." + ext]