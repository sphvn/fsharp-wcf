﻿open System
open System.IO 

open System.Net
open System.Text
open System.ServiceModel
open System.ServiceModel.Web

let basicResponse(html:string) =
  WebOperationContext.Current.OutgoingResponse.ContentType <- "text/html"
  new MemoryStream (Encoding.UTF8.GetBytes html)

[<ServiceContract>]
type MyContract() =
    [<OperationContract>]
    [<WebGet(UriTemplate="{a}")>]
    member this.Single(a:string) : Stream =
        let html = sprintf "Called with '%s'" a
        upcast basicResponse html

    [<OperationContract>]
    [<WebGet(UriTemplate="{a}/{b}")>]
    member this.Double(a:string) (b:string) : Stream =
        let html = sprintf "Called with '%s' and '%s'" a b
        upcast basicResponse html

    [<OperationContract>]
    [<WebGet(UriTemplate="static/{*path}")>]
    member this.Static(path:string) : Stream =
        let res = WebOperationContext.Current.OutgoingResponse
        let path = "static/" + path

        if (File.Exists(path)) then
          res.ContentType <- Mime.getMimeType (Path.GetExtension path)
          res.StatusCode <- HttpStatusCode.OK
          upcast File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
        else
          res.StatusCode <- HttpStatusCode.NotFound
          upcast basicResponse "Error, file not found"

let Main() =
    let address = "http://localhost:8000/"
    let host = new WebServiceHost (typeof<MyContract>, new Uri(address))
    host.AddServiceEndpoint (typeof<MyContract>, new WebHttpBinding(), "") |> ignore
    host.Open()
    printfn "ServiceHost started %s" address
    System.Console.ReadKey() |> ignore
    host.Close()

Main()
